const fs = require('fs');
const express = require('express');
const cookieParser = require('cookie-parser');
const dotenv = require('dotenv');

dotenv.config({ path: './config.env' });

const app = express();

app.set('views', `${__dirname}/views`);
app.set('view engine', 'ejs');

app.use(express.static(`${__dirname}/public`));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

const getLanguage = (req, res, next) => {

    //seulement si cookie pas déjà défini
    if (req.cookies.lang === 'fr') {
        req.lang = 'fr';
        return next();
    }

    if (req.cookies.lang === 'en') {
        req.lang = 'en';
        return next();
    }

    req.lang = 'en';

    const acceptedLang = req.acceptsLanguages();

    if (acceptedLang[0] === 'fr' || acceptedLang[0] === 'fr-FR') req.lang = 'fr';

    const cookieOptions = {
        expires: new Date(Date.now() + process.env.LANG_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
        httpOnly: true,
        sameSite: 'Lax'
        //secure: true
    };

    res.cookie('lang', req.lang, cookieOptions);
    
    next();
};

app.get('/', getLanguage, (req, res) => {

    const homePath = `${req.lang}/home`;

    res.status(200).render(homePath, {
        title: 'Louis Daniel | Portfolio',
        language: req.lang
    });

});

app.post('/', getLanguage, (req, res) => {

    const newLang = req.body.lang.toLowerCase();
    const oldLang = req.lang;

    if (!newLang) {

        res.status(401).json({
            status: 'fail',
            data: {
                message: 'Please, provide a new language!'
            }
        });

        return;
    }

    if (newLang !== 'en' && newLang !== 'fr') {
        
        res.status(401).json({
            status: 'fail',
            data: {
                message: 'Please, provide a correct language!'
            }
        });

        return;
    }

    if (newLang === oldLang) {

        res.status(401).json({
            status: 'fail',
            data: {
                message: 'Please, choose a different language than the current one!'
            }
        });

        return;
    }

    res.cookie('lang', newLang);

    res.status(200).json({
        status: 'success',
        data: {
            message: 'Language successfully changed!'
        }
    });

});

app.get('/resume', getLanguage, (req, res) => {

    let resumePath;

    if (req.lang === 'en') resumePath = `${__dirname}/views/${req.lang}/Resume_DANIEL_Louis.pdf`;

    if (req.lang === 'fr') resumePath = `${__dirname}/views/${req.lang}/CV_DANIEL_Louis.pdf`;

    fs.readFile(resumePath, (err, data) => {
        res.contentType('application/pdf');

        res.send(data);
    });

});


/**
 * Error 404
 */
/* app.all('*', (req, res) => {
    
    res.status(404).json({
        status: 'fail',
        data: {
            message: 'This page does not exist on this server'
        }
    });

}); */

const port = 3000;

app.listen(port, () => {
    console.log(`App running on port ${port}...`);
});